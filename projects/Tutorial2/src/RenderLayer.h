#pragma once
#include "florp/app/ApplicationLayer.h"

class RenderLayer {

	virtual void onEnter() override;
	virtual void PreRender() override;
	virtual void Render() override;


};
