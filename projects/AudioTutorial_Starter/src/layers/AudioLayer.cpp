#include "AudioLayer.h"
#include "AudioEngine.h"

void AudioLayer::Initialize()
{
	// TODO: Init the sound engine and load the master bank
	AudioEngine& audioEngine = AudioEngine::GetInstance();

	AudioEngine::GetInstance().Init();
	AudioEngine::GetInstance().LoadBank("Master");

	//audioEngine.LoadEvent("Music");
	//audioEngine.PlayEvent("Music");


}

void AudioLayer::Shutdown()
{
	//TODO: Shutdown AudioEngine
	AudioEngine::GetInstance().Shutdown();

}

void AudioLayer::Update()
{
	//TODO: Update AudioEngine
	AudioEngine::GetInstance().Update();
}