#include "RenderLayer.h"
#include <florp\game\SceneManager.h>
#include <florp\game\RenderableComponent.h>
#include <florp\app\Timing.h>
#include <florp\game\Transform.h>
#include <CameraComponent.h>
#include <florp/app/Application.h>
#include "Texture3D.h"

typedef florp::game::RenderableComponent Renderable;

//checks if certain keys were pressed
static bool is1Pressed = false; 
static bool is2Pressed = false;
static bool is3Pressed = false;
static bool is4Pressed = false;
static bool is5Pressed = false;
static bool is6Pressed = false;
static bool is7Pressed = false;
static bool is8Pressed = false;
static bool is9Pressed = false;
static bool is0Pressed = false;
static bool isEPressed = false;

void sortRenderers(entt::registry& reg) {
	// We sort our mesh renderers based on material properties
	// This will group all of our meshes based on shader first, then material second
	reg.sort<florp::game::RenderableComponent>([](const florp::game::RenderableComponent& lhs, const florp::game::RenderableComponent& rhs) {
		if (rhs.Material == nullptr || rhs.Mesh == nullptr)
			return false;
		else if (lhs.Material == nullptr || lhs.Mesh == nullptr)
			return true;
		else if (lhs.Material->RasterState.Blending.BlendEnabled & !rhs.Material->RasterState.Blending.BlendEnabled)
			return false;
		else if (!lhs.Material->RasterState.Blending.BlendEnabled & rhs.Material->RasterState.Blending.BlendEnabled)
			return true;
		else if (lhs.Material->GetShader() != rhs.Material->GetShader())
			return lhs.Material->GetShader() < rhs.Material->GetShader();
		else
			return lhs.Material < rhs.Material;
		});
}

void ctorSort(entt::entity, entt::registry& ecs, const Renderable& r) {
	sortRenderers(ecs);
}
void dtorSort(entt::entity, entt::registry& ecs) {
	sortRenderers(ecs);
}

void RenderLayer::OnWindowResize(uint32_t width, uint32_t height)
{
	CurrentRegistry().view<CameraComponent>().each([&](auto entity, CameraComponent& cam) {
		if (cam.IsMainCamera)
			cam.Buffer->Resize(width, height);
	});
}

void RenderLayer::OnSceneEnter() {
	CurrentRegistry().on_construct<Renderable>().connect<&::ctorSort>();
	CurrentRegistry().on_destroy<Renderable>().connect<&::dtorSort>();

	//load LUTs
	florp::graphics::Texture3D::Sptr coolLUT = florp::graphics::Texture3D::LoadFromFile("./luts/Cool.CUBE");
	coolLUT->Bind(3);
	florp::graphics::Texture3D::Sptr warmLUT = florp::graphics::Texture3D::LoadFromFile("./luts/Warm.CUBE");
	warmLUT->Bind(4);
	florp::graphics::Texture3D::Sptr customLUT = florp::graphics::Texture3D::LoadFromFile("./luts/Custom.CUBE");
	customLUT->Bind(5);

}

void RenderLayer::Render()
{
	auto& ecs = CurrentRegistry();

	florp::game::Material::Sptr material = nullptr;
	florp::graphics::Shader::Sptr boundShader = nullptr;

	florp::app::Application* app = florp::app::Application::Get();

	CurrentRegistry().sort<CameraComponent>([](const CameraComponent& lhs, const CameraComponent& rhs) {
		return rhs.IsMainCamera;
		});

	CurrentRegistry().view<CameraComponent>().each([&](auto entity, CameraComponent& cam) {
		florp::game::Transform& camTransform = ecs.get<florp::game::Transform>(entity);

		//move camera
		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_W)) {
			camTransform.SetEulerAngles(glm::vec3(camTransform.GetLocalEulerAngles().x + 1,
				camTransform.GetLocalEulerAngles().y, camTransform.GetLocalEulerAngles().z));
		}
		else if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_S)) {
			camTransform.SetEulerAngles(glm::vec3(camTransform.GetLocalEulerAngles().x - 1,
				camTransform.GetLocalEulerAngles().y, camTransform.GetLocalEulerAngles().z));
		}
		else if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_A)) {
			camTransform.SetEulerAngles(glm::vec3(camTransform.GetLocalEulerAngles().x,
				camTransform.GetLocalEulerAngles().y + 1, camTransform.GetLocalEulerAngles().z));
		}
		else if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_D)) {
			camTransform.SetEulerAngles(glm::vec3(camTransform.GetLocalEulerAngles().x,
				camTransform.GetLocalEulerAngles().y - 1, camTransform.GetLocalEulerAngles().z));
		}
		else if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_Q)) {
			camTransform.SetPosition(glm::vec3(camTransform.GetLocalPosition().x,
				camTransform.GetLocalPosition().y, camTransform.GetLocalPosition().z + 0.01));
		}
		else if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_E)) {
			camTransform.SetPosition(glm::vec3(camTransform.GetLocalPosition().x,
				camTransform.GetLocalPosition().y, camTransform.GetLocalPosition().z - 0.01));
		}

		//toggle scene based on keys
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad1) == florp::app::ButtonState::Down) {
			if (!is1Pressed)
			{
				ambientOnly = 0;
				ambient = 0;
				specular = 0;
				rim = 0;
				diffuse = 0;
			}
			is1Pressed = true;
		} else {
			is1Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad2) == florp::app::ButtonState::Down) {
			if (!is2Pressed)
			{
				ambientOnly = 1;
				ambient = 0;
				specular = 0;
				rim = 0;
				diffuse = 0;
			}
			is2Pressed = true;
		}
		else {
			is2Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad3) == florp::app::ButtonState::Down) {
			if (!is3Pressed)
			{
				ambientOnly = 0;
				ambient = 0;
				specular = 1;
				rim = 0;
				diffuse = 0;
			}
			is3Pressed = true;
		}
		else {
			is3Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad4) == florp::app::ButtonState::Down) {
			if (!is4Pressed)
			{
				ambientOnly = 0;
				ambient = 0;
				specular = 1;
				rim = 1;
				diffuse = 0;
			}
			is4Pressed = true;
		}
		else {
			is4Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad5) == florp::app::ButtonState::Down) {
			if (!is5Pressed)
			{
				ambientOnly = 0;
				ambient = 1;
				specular = 1;
				rim = 1;
				diffuse = 0;
			}
			is5Pressed = true;
		} else {
			is5Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad6) == florp::app::ButtonState::Down) {
			if (!is6Pressed)
			{
				if (specular == true) {
					diffuseRamp = !diffuseRamp;
				}
			}
			is6Pressed = true;
		}
		else {
			is6Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad7) == florp::app::ButtonState::Down) {
			if (!is7Pressed)
			{
				if (specular == true) {
					specularRamp = !specularRamp;
				}
			}
			is7Pressed = true;
		}
		else {
			is7Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad8) == florp::app::ButtonState::Down) {
			if (!is8Pressed)
			{
				warm = !warm;
			}
			is8Pressed = true;
		}
		else {
			is8Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad9) == florp::app::ButtonState::Down) {
			if (!is9Pressed)
			{
				cool = !cool;
			}
			is9Pressed = true;
		}
		else {
			is9Pressed = false;
		}
		if (app->GetWindow()->GetKeyState(florp::app::Key::Keypad0) == florp::app::ButtonState::Down) {
			if (!is0Pressed)
			{
				custom = !custom;
			}
			is0Pressed = true;
		}
		else {
			is0Pressed = false;
		}
		//reset lighting to normal
		if (app->GetWindow()->GetKeyState(florp::app::Key::KeypadEnter) == florp::app::ButtonState::Down) {
			if (!isEPressed)
			{
				ambientOnly = 0;
				ambient = 1;
				specular = 1;
				diffuse = 1;
				rim = 1;
				diffuseRamp = 1;
				specularRamp = 1;
				warm = 0;
				cool = 0;
				custom = 0;
			}
			isEPressed = true;
		}
		else {
			isEPressed = false;
		}


		if (cam.IsMainCamera)
			ecs.set<FrameBuffer::Sptr>(cam.Buffer);

		cam.Buffer->Bind();
		glViewport(0, 0, cam.Buffer->GetWidth(), cam.Buffer->GetHeight());
		glClearColor(cam.ClearCol.x, cam.ClearCol.y, cam.ClearCol.z, cam.ClearCol.w);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		
		glm::vec3 position = camTransform.GetLocalPosition();
		glm::mat4 viewMatrix = glm::inverse(camTransform.GetWorldTransform());
		glm::mat4 viewProjection = cam.Projection * viewMatrix;
		
		// A view will let us iterate over all of our entities that have the given component types
		auto view = ecs.view<Renderable>();

		for (const auto& entity : view) {

			// Get our shader
			const Renderable& renderer = ecs.get<Renderable>(entity);

			// Early bail if mesh is invalid
			if (renderer.Mesh == nullptr || renderer.Material == nullptr)
				continue;

			// If our shader has changed, we need to bind it and update our frame-level uniforms
			if (renderer.Material->GetShader() != boundShader) {
				boundShader = renderer.Material->GetShader();
				boundShader->Use();
				boundShader->SetUniform("a_CameraPos", position);
				boundShader->SetUniform("a_Time", florp::app::Timing::GameTime);
			}
			
			// If our material has changed, we need to apply it to the shader
			if (renderer.Material != material) {
				material = renderer.Material;
				material->Apply(); 
			}

			// We'll need some info about the entities position in the world
			const florp::game::Transform& transform = ecs.get_or_assign<florp::game::Transform>(entity);

			// Our normal matrix is the inverse-transpose of our object's world rotation
			glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(transform.GetWorldTransform())));

			// Update the MVP using the item's transform
			boundShader->SetUniform(
				"a_ModelViewProjection",
				viewProjection *
				transform.GetWorldTransform());

			// Update the model matrix to the item's world transform
			boundShader->SetUniform("a_Model", transform.GetWorldTransform());

			// Update the model matrix to the item's world transform
			boundShader->SetUniform("a_NormalMatrix", normalMatrix); 
		
			//lighting uniforms
			boundShader->SetUniform("ambientOn", ambient);
			boundShader->SetUniform("ambientOnly", ambientOnly);
			boundShader->SetUniform("specularOn", specular);
			boundShader->SetUniform("rimOn", rim);
			boundShader->SetUniform("diffuseOn", diffuse);
			boundShader->SetUniform("diffuseRampOn", diffuseRamp);
			boundShader->SetUniform("diffuseRampOff", !diffuseRamp);
			boundShader->SetUniform("specularRampOn", specularRamp);
			boundShader->SetUniform("specularRampOff", !specularRamp);
			boundShader->SetUniform("coolOn", cool);
			boundShader->SetUniform("warmOn", warm);
			boundShader->SetUniform("customOn", custom);

			//luts
			boundShader->SetUniform("cool", 3);
			boundShader->SetUniform("warm", 4);
			boundShader->SetUniform("custom", 5);


			// Draw the item
			renderer.Mesh->Draw();
		}
		cam.Buffer->UnBind();
	});
}
