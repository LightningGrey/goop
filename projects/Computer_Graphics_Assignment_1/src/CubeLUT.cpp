#include "CubeLUT.h"
#include <iostream>
#include <sstream>

std::string CubeLUT::ReadLine(std::ifstream& infile, char lineSeparator)
{
	//skip empty lines and comments
	//only data
	const char commentMarker = '#';
	std::string textLine("");

	while (textLine.size() == 0 || textLine[0] == commentMarker) {
		if (infile.eof()) {
			status = PrematureEndOfFile;
			break;
		}
		std::getline(infile, textLine);
		if (infile.fail()) {
			status = ReadError;
			break;
		}
	}

	return textLine;
}

std::vector<float> CubeLUT::ParseTableRow(const std::string& lineOfText)
{
	int n = 3;
	tableRow returnRow(n);
	std::istringstream line(lineOfText);
	for (int i = 0; i < n; i++) {
		line >> returnRow[i];
		if (line.fail()) {
			status = CouldNotParseTableData;
			break;
		}
	}
	return returnRow;
}

CubeLUT::LUTState CubeLUT::LoadCubeFile(std::ifstream& infile)
{
	status = OK;
	title.clear();
	//set min and max
	domainMin = tableRow(3, 0.0);
	domainMax = tableRow(3, 1.0);
	//clear last LUT
	LUT1D.clear();
	LUT3D.clear();

	const char newline = '\n';
	char lineSeparator = newline;

	const char carriageReturn = '\r';
	for (int i = 0; i < 255; i++) {
		char c = infile.get();
		if (c == newline) {
			break;
		}
		if (c == carriageReturn) {
			if (infile.get() == newline) {
				break;
			}
			lineSeparator = carriageReturn;
			std::clog << "INFO: File uses non-compliant line separator" << std::endl;
			break;
		}
		if (i > 250) {
			status = LineError; 
			break;
		}
	}
	infile.seekg(0);
	infile.clear();

	//keywords to read
	int n, titleC, sizeC, minC, maxC;
	n = titleC = sizeC = minC = maxC = 0;

	while (status == OK) {
		long linePos = infile.tellg();
		std::string textLine = ReadLine(infile, lineSeparator);
		if (!status == OK) {
			break;
		}

		std::istringstream line(textLine);
		std::string keyword;
		line >> keyword;

		if ("+" < keyword && keyword < ":") {
			infile.seekg(linePos);
			break;
		}
		else if (keyword == "TITLE" && titleC++ == 0) {
			const char quote = '""';
			char titleStart;
			line >> titleStart;
			if (titleStart != quote) {
				status = TitleMissingQuote;
				break;
			}
			std::getline(line, title, quote);
		}
		else if (keyword == "DOMAIN_MIN" && minC++ == 0) {
			line >> domainMin[0] >> domainMin[1] >> domainMin[2];
		}
		else if (keyword == "DOMAIN_MAX" && maxC++ == 0) {
			line >> domainMax[0] >> domainMax[1] >> domainMax[2];
		}
		else if (keyword == "LUT_1D_SIZE" && sizeC++ == 0) {
			line >> n;
			size = n;
			if (n < 2 || n > 65536) {
				status = LUTSizeOutOfRange;
				break;
			}
			LUT1D = table1D(n, tableRow(3));
		}
		else if (keyword == "LUT_3D_SIZE" && sizeC++ == 0) {
			line >> n;
			size = n;
			if (n < 2 || n > 256) {
				status = LUTSizeOutOfRange;
				break;
			}
			LUT3D = table3D(n, table2D(n, table1D(n, tableRow(3))));
		}
		else {
			status = UnknownOrRepeatedKeyword;
			break;
		}

		if (line.fail()) {
			status = ReadError;
			break;
		}
	}

	if (status == OK && sizeC == 0) {
		status == LUTSizeOutOfRange;
	}

	if (status == OK && (domainMin[0] >= domainMax[0] ||
		domainMin[1] >= domainMax[1] ||
		domainMin[2] >= domainMax[2])) {
		status = DomainBoundsReversed;
	}

	//read lines of data
	if (LUT1D.size() > 0) {
		n = LUT1D.size();
		for (int i = 0; i < n && status == OK; i++) {
			LUT1D[i] = ParseTableRow(ReadLine(infile, lineSeparator));
		}
	} else {
		//3D LUT
		n = LUT3D.size();
		for (int b = 0; b < n && status == OK; b++) {
			for (int g = 0; g < n && status == OK; g++) {
				for (int r = 0; r < n && status == OK; r++) {
					LUT3D[r][g][b] = ParseTableRow(ReadLine(infile, lineSeparator));
				}
			}
		}
	}

	return status;
}

CubeLUT::LUTState CubeLUT::SaveCubeFile(std::ofstream& outfile)
{
	if (!status == OK) {
		return status;
	}

	const char space = ' ';
	const char quote = '"';

	if (title.size() > 0) {
		outfile << "TITLE" << space << quote << title << quote << std::endl;
	}
	outfile << "# Created by CubeLUT.cpp" << std::endl;
	outfile << "DOMAIN_MIN" << space << domainMin[0] << space << domainMin[1]
		<< space << domainMax[2] << std::endl;
	outfile << "DOMAIN_MAX" << space << domainMax[0] << space << domainMax[1]
		<< space << domainMax[2] << std::endl;
	
	//write LUT data
	if (LUT1D.size() > 0) {
		int n = LUT1D.size();
		outfile << "LUT_1D_SIZE" << space << n << std::endl;
		for (int i = 0; i < n && outfile.good(); i++) {
			outfile << LUT1D[i][0] << space << LUT1D[i][1] << space <<
				LUT1D[i][2] << std::endl;
		}
	}
	else {
		int n = LUT3D.size();
		outfile << "LUT_3D_SIZE" << space << n << std::endl;
		for (int b = 0; b < n && outfile.good(); b++) {
			for (int g = 0; g < n && outfile.good(); g++) {
				for (int r = 0; r < n && outfile.good(); r++) {
					outfile << LUT3D[r][g][b][0] << space << LUT3D[r][g][b][1] <<
						space << LUT3D[r][g][b][2] << std::endl;
				}
			}

		}
	}

	outfile.flush();
	return (outfile.good() ? OK : WriteError);
}


