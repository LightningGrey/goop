#pragma once
#include "florp/app/ApplicationLayer.h"

class RenderLayer : public florp::app::ApplicationLayer
{
public:
	virtual void OnWindowResize(uint32_t width, uint32_t height) override;

	virtual void OnSceneEnter() override;

	// Render will be where we actually perform our rendering
	virtual void Render() override;

	//uniform check bools
	bool ambient = 1;
	bool ambientOnly = 0;
	bool specular = 1;
	bool diffuse = 1;
	bool rim  = 1;
	bool diffuseRamp = 1;
	bool specularRamp = 1;
	bool warm = 0;
	bool cool = 0;
	bool custom = 0;

};
