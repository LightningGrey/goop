INFR 2350 - Assignment 1
Hunter Chu - 10701653
Edward Cao - 100697845


CONTROLS:
W - rotate view up
S - rotate view down
A - rotate view left
D - rotate view right
Q - move backward
E - move forward

Numpad controls:
1 - no lighting
2 - ambient only
3 - specular only
4 - specular and rim
5 - ambient, specular, and rim
6 - toggle diffuse ramp
7 - toggle specular ramp
8 - toggle warm mode
9 - toggle cool mode
0 - toggle custom mode
Enter - reset to original scene (all lighting and ramp terms)


NOTES:
Colour correction did not work, LUT was loaded but texture was unable to be loaded
colourCorrection.fs.glsl was intended to be used but did not work
was tested in blinn-phong-multi.fs.glsl

Files that were created:
Texture1D
Texture3D
cone.obj
cube.obj
sphere.obj
colourCorrection.fs.glsl

Files from GOOP that had changes made:
SceneBuildLayer
RenderLayer
CameraComponent (only adjustment was changing the camera colour, colour change not reflected in report images)
blinn-phong-multi.fs.glsl

All other files were retained from GOOP