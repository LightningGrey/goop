#include "SceneBuildLayer.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>
#include <glm/gtc/matrix_transform.hpp>
#include <florp\game\Transform.h>

#include "CameraComponent.h"
#include <florp\app\Application.h>
#include "SampleBehaviour.h"
#include "LightBehaviour.h"

void SceneBuilder::Initialize()
{
	using namespace florp::game;
	using namespace florp::graphics;
	
	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	MeshData data = ObjLoader::LoadObj("monkey.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong-multi.fs.glsl");
	shader->Link();

	Material::Sptr mat = std::make_shared<Material>(shader);
	mat->Set("a_LightPos", { 2, 0, 4 });
	mat->Set("a_LightColor", { 1.0f, 1.0f, 1.0f });
	mat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	mat->Set("a_AmbientPower", 0.1f);
	mat->Set("a_LightSpecPower", 0.5f);
	mat->Set("a_LightShininess", 256.0f);
	mat->Set("a_LightAttenuation", 1.0f / 100.0f);

	mat->Set("a_Lights[0].Pos", { 2, 0, 0 });
	mat->Set("a_Lights[0].Color", { 1.0f, 0.0f, 1.0f });
	mat->Set("a_Lights[0].Attenuation", 1.0f / 100.0f);
	mat->Set("a_Lights[1].Pos", { -2, 0, 0 });
	mat->Set("a_Lights[1].Color", { 0.0f, 1.0f, 0.0f });
	mat->Set("a_Lights[1].Attenuation", 1.0f / 100.0f);
	mat->Set("a_EnabledLights", 2);

	Material::Sptr mat2 = std::make_shared<Material>(shader);
	//mat2->Set("a_LightPos", { 2, 0, 4 });
	//mat2->Set("a_LightColor", { 1.0f, 1.0f, 1.0f });
	//mat2->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	//mat2->Set("a_AmbientPower", 0.1f);
	//mat2->Set("a_LightSpecPower", 0.5f);
	//mat2->Set("a_LightShininess", 256.0f);
	//mat2->Set("a_LightAttenuation", 1.0f / 100.0f);

	mat2->Set("a_Lights[0].Pos", { 1, 1, 1 });
	mat2->Set("a_Lights[0].Color", { 1.0f, 0.0f, 1.0f });
	mat2->Set("a_Lights[0].Attenuation", 1.0f / 100.0f);
	mat2->Set("a_Lights[1].Pos", { -2, 0, 0 });
	mat2->Set("a_Lights[1].Color", { 1.0f, 0.0f, 0.0f });
	mat2->Set("a_Lights[1].Attenuation", 1.0f / 100.0f);
	mat2->Set("a_EnabledLights", 2);

	{
		entt::entity test = scene->CreateEntity();

		//scene->AddBehaviour<SampleBehaviour>(test, "Hello world!");
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
		
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = mat;
		Transform& t = scene->Registry().get<Transform>(test);
		scene->AddBehaviour<SampleBehaviour>(test, glm::vec3(0, 45.0f, 90.0f));
		scene->AddBehaviour<LightBehaviour>(test);
		t.SetPosition(glm::vec3(0, 0, -10));
	}

	//{
	//	entt::entity light = scene->CreateEntity();
	//	//scene->AddBehaviour<SampleBehaviour>(test, "Hello world!");
	//	RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
	//	renderable.Material = mat;
	//	Transform& t = scene->Registry().get<Transform>(test);
	//	light->AddBehaviour<SampleBehaviour>(test, glm::vec3(0, 45.0f, 90.0f));
	//	t.SetPosition(glm::vec3(0, 0, -10));
	//}


	{
		florp::app::Application* app = florp::app::Application::Get();

		// The color buffer should be marked as shader readable, so that we generate a texture for it
		RenderBufferDesc mainColor = RenderBufferDesc();
		mainColor.ShaderReadable = true;
		mainColor.Attachment = RenderTargetAttachment::Color0;
		mainColor.Format = RenderTargetType::SColor32;
		
		// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
		RenderBufferDesc depth = RenderBufferDesc();
		depth.Attachment = RenderTargetAttachment::DepthStencil;
		depth.Format = RenderTargetType::DepthStencil;
		
		// Our main frame buffer needs a color output, and a depth output
		FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		buffer->AddAttachment(mainColor);
		buffer->AddAttachment(depth);
		buffer->Validate();

		entt::entity camera = scene->CreateEntity();
		CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
		cam.Buffer = buffer;
		cam.IsMainCamera = true;
		cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
		Transform& t = scene->Registry().get<Transform>(camera);
		t.SetPosition(glm::vec3(0.0f, 0.0f, -5.0f));
	}
}
