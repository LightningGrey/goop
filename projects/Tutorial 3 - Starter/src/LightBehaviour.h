#pragma once
#include "florp/game/IBehaviour.h"
#include "Logging.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "imgui.h"

class LightBehaviour : public florp::game::IBehaviour {
public:
	LightBehaviour(const florp::game::Material::Sptr& mat) : IBehaviour(), light(mat) {};
	virtual ~LightBehaviour() = default;

	virtual void Update(entt::entity entity) override {
		//auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		auto& mat = CurrentRegistry().get<florp::game::Material::Sptr>(entity);
		CurrentRegistry().set<florp::game::Material::Sptr>(entity);
	}

private:
	florp::game::Material::Sptr light;
	float x=0;
	float y=0;
	float z=0;
};

