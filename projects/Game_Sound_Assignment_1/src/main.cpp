#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "TTK/TTKContext.h"
#include "Logging.h"
#include "TTK/GraphicsUtils.h"
#include "TTK/Camera.h"
#include "AudioEngine.h"
#include "florp/graphics/ObjLoader.h"
#include "florp/graphics/Mesh.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include "TTK/Input.h"

#include "imgui.h"
#include "TTK/SpriteSheetQuad.h"
#define PI 3.14159265

#define LOG_GL_NOTIFICATIONS
/*
	Handles debug messages from OpenGL
	https://www.khronos.org/opengl/wiki/Debug_Output#Message_Components
	@param source    Which part of OpenGL dispatched the message
	@param type      The type of message (ex: error, performance issues, deprecated behavior)
	@param id        The ID of the error or message (to distinguish between different types of errors, like nullref or index out of range)
	@param severity  The severity of the message (from High to Notification)
	@param length    The length of the message
	@param message   The human readable message from OpenGL
	@param userParam The pointer we set with glDebugMessageCallback (should be the game pointer)
*/
void GlDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	std::string sourceTxt;
	switch (source) {
	case GL_DEBUG_SOURCE_API: sourceTxt = "DEBUG"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM: sourceTxt = "WINDOW"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: sourceTxt = "SHADER"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY: sourceTxt = "THIRD PARTY"; break;
	case GL_DEBUG_SOURCE_APPLICATION: sourceTxt = "APP"; break;
	case GL_DEBUG_SOURCE_OTHER: default: sourceTxt = "OTHER"; break;
	}
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:          LOG_INFO("[{}] {}", sourceTxt, message); break;
	case GL_DEBUG_SEVERITY_MEDIUM:       LOG_WARN("[{}] {}", sourceTxt, message); break;
	case GL_DEBUG_SEVERITY_HIGH:         LOG_ERROR("[{}] {}", sourceTxt, message); break;
#ifdef LOG_GL_NOTIFICATIONS
	case GL_DEBUG_SEVERITY_NOTIFICATION: LOG_INFO("[{}] {}", sourceTxt, message); break;
#endif
	default: break;
	}
}

int main() 
{ 
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	long long memBreak = 0;
	if (memBreak) _CrtSetBreakAlloc(memBreak);

	Logger::Init();

	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return 1;
	}


	// Create a new GLFW window
	GLFWwindow* window = glfwCreateWindow(600, 600, "Assignment #1 - 100701653", nullptr, nullptr);

	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(window);

	// Initialize the TTK input system
	TTK::Input::Init(window);

	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return 2;
	}

	// Display our GPU and OpenGL version
	std::cout << glGetString(GL_RENDERER) << std::endl;
	std::cout << glGetString(GL_VERSION) << std::endl;

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(GlDebugMessage, nullptr);

	TTK::Graphics::SetCameraMode3D(600, 600);
	glfwSetWindowSizeCallback(window, [](GLFWwindow* window, int width, int height) {
		TTK::Graphics::SetCameraMode3D(width, height);
		});
	TTK::Graphics::SetBackgroundColour(0.5f, 0.5f, 0.5f);
	TTK::Graphics::SetDepthEnabled(true);

	//camera positioning, smaller sphere in direction of larger sphere's front
	TTK::Camera camera;
	camera.forwardVector = glm::vec3(1, -1, 1);
	camera.cameraPosition = glm::vec3(-15, 15, -15);
	
	TTK::Graphics::InitImGUI(window);

	float lastFrame = glfwGetTime();

	//angle and position of moving sphere and sound
	float angle = 0.0f;
	float rotX = 0.0f;
	float rotZ = 5.0f;
	float moveZ = 5.0f;

	//increment for moving sphere back and forth
	float increment = 1.0f;

	//current rolloff
	int rolloffVal = 0;
	std::string rolloffType = "INVERSE";

	//create engine object
	AudioEngine engine;

	engine.Init();

	//load bank and event
	engine.LoadBank("Master", FMOD_STUDIO_LOAD_BANK_NORMAL);

	engine.LoadEvent("Music", "{7bd6f2d3-8215-458d-be4a-d8fcdb22296d}");

	//// Play the event
	engine.PlayEvent("Music");
	
	//set starting position for sphere and event
	glm::vec3 startPosition = { 0, 0, 5 };
	engine.SetEventPosition("Music", startPosition);

	//records current key effect in place
	TTK::KeyCode currentKey = TTK::KeyCode::I;


	// Run as long as the window is open
	while (!glfwWindowShouldClose(window)) {
		// Poll for events from windows (clicks, keypressed, closing, all that)
		glfwPollEvents();

		float thisFrame = glfwGetTime();
		float dt = thisFrame - lastFrame;

		// Clear our screen every frame
		TTK::Graphics::ClearScreen();

		//move camera (though not necessary)
		if (TTK::Input::GetKeyDown(TTK::KeyCode::W))
			camera.moveForward();
		if (TTK::Input::GetKeyDown(TTK::KeyCode::S))
			camera.moveBackward();
		if (TTK::Input::GetKeyDown(TTK::KeyCode::A))
			camera.moveLeft();
		if (TTK::Input::GetKeyDown(TTK::KeyCode::D))
			camera.moveRight();
		if (TTK::Input::GetKeyDown(TTK::KeyCode::LeftControl))
			camera.moveDown();
		if (TTK::Input::GetKeyDown(TTK::KeyCode::Space))
			camera.moveUp();

		//reset camera
		if (TTK::Input::GetKeyPressed(TTK::KeyCode::R))
			camera.cameraPosition = glm::vec3(-15, 15, -15);

		//set rotation
		if (TTK::Input::GetKeyPressed(TTK::KeyCode::P)) {
			moveZ = 5.0f;
			increment = 1.0f;
			currentKey = TTK::KeyCode::P;
		}

		//set moving back and forth
		if (TTK::Input::GetKeyPressed(TTK::KeyCode::O)) {
			angle = 0.0f;
			currentKey = TTK::KeyCode::O;
		}

		//reset to base 
		if (TTK::Input::GetKeyPressed(TTK::KeyCode::I)) {
			angle = 0.0f;
			moveZ = 5.0f;
			increment = 1.0f;
			currentKey = TTK::KeyCode::I;
		}

		//changes rolloff between inverse and linear 
		if (TTK::Input::GetKeyPressed(TTK::KeyCode::L)) {
			rolloffVal += 1;
			if (rolloffVal == 1) {
				rolloffType = "LINEAR";
			}
			else if (rolloffVal == 2) {
				rolloffType = "LINEAR SQUARE";
			} 
			else if (rolloffVal == 3) {
				rolloffType = "INVERSE TAPERED";
			}
			else {
				rolloffVal = 0;
				rolloffType = "INVERSE";
			}
			engine.SetMode(0, rolloffType);
		}


		camera.update();
		TTK::Graphics::SetCameraMatrix(camera.ViewMatrix);

		TTK::Graphics::DrawGrid();
		TTK::Graphics::DrawLine(glm::vec3(0.0f, 100.0f, 0.0f),
			glm::vec3(0.0f, -100.0f, 0.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
		TTK::Graphics::DrawSphere(glm::vec3(0, 0.0f, 0), 2.0f);

		//rotate around counterclockwise, sphere facing forward in direction of smaller sphere
		if (currentKey == TTK::KeyCode::P) {
			angle -= 5;
			rotX = sin(angle * PI / 180) * 5;
			rotZ = cos(angle * PI / 180) * 5;

			engine.SetEventPosition("Music", glm::vec3(rotX, 0, rotZ));
			TTK::Graphics::DrawSphere(glm::vec3(-rotX, 0, rotZ), 1.0f);
		}
		else if (currentKey == TTK::KeyCode::O) {
			moveZ += increment;
			engine.SetEventPosition("Music", startPosition + glm::vec3(0, 0, moveZ));
			TTK::Graphics::DrawSphere(startPosition + glm::vec3(0, 0, moveZ), 1.0f);
			if (moveZ > 15.0f || moveZ < 1.0f) {
				increment = -increment;
			}
		}
		else {
			TTK::Graphics::DrawSphere(startPosition, 1.0f);
			engine.SetEventPosition("Music", startPosition);
		}


		engine.Update();

		//write label of rolloff type on screen
		TTK::Graphics::DrawText2D("ROLLOFF TYPE: " + rolloffType, 15, 30, 35);

		TTK::Graphics::EndFrame();
		TTK::Graphics::BeginGUI();


		TTK::Graphics::EndGUI();

		// Present our image to windows
		glfwSwapBuffers(window);

		TTK::Input::Poll();

		lastFrame = thisFrame;

		Sleep(100);

	}

	glfwTerminate();

	//// Shut down audio engine
	engine.Shutdown();

	TTK::Graphics::ShutdownImGUI();
	TTK::Input::Uninitialize();
	TTK::Graphics::Cleanup();
	Logger::Uninitialize();

	return 0; 
} 
