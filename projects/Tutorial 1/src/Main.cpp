#include "GLFW/glfw3.h"
#include "glad/glad.h"
#include <iostream>

const char* vertexShaderSource = "#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"		//pos variable in slot 0
"layout (location = 1) in vec3 aColour;\n"	//colour variable in slot 1
"out vec3 triColour;\n"
"void main()\n"
"{\n"
"gl_Position = vec4(aPos, 1.0);\n"
"triColour = aColour;\n"
"}\0";

const char* fragmentShaderSource = "#version 330 core\n"
"out vec4 FragColour;\n"
"in vec3 triColour;\n"
"void main()\n"
"{\n"
"FragColour = vec4(triColour, 1.0f);\n"
"}\n\0";


int main() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//initialize GLFW
	if (glfwInit == GLFW_FALSE){
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}

	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

	GLFWwindow* myWindow = glfwCreateWindow(800, 800, "what am i fighting forrrrrr", nullptr, nullptr);
	glfwMakeContextCurrent(myWindow);

	//initialize glad
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	//create VAO/VBO
	float triangle[] = {
		//triangle				//colours
		-0.5f, -0.5f, 0.0f,		1.0f, 0.0f, 0.0f,	//left
		0.5f, -0.5f, 0.0f,		0.0f, 1.0f, 0.0f,	//right
		0.0f, 0.5f, 0.0f,		0.0f, 0.0f, 1.0f,	//top
	};

	unsigned int VBO, VAO;
	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VAO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

	//vert pointer
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//create shaders
	int success;
	unsigned int vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertShader);
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &success);
	unsigned int fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragShader);
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);


	unsigned int shaderProgram = glCreateProgram();
	//glAttachShader
	
	glAttachShader(shaderProgram, vertShader);
	glAttachShader(shaderProgram, fragShader);
	glLinkProgram(shaderProgram);
	glGetProgramiv(shaderProgram, GL_COMPILE_STATUS, &success);
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	//while window is open

	//bind the VAO
	//draw stuff
	//poll
	//swap
	while(!glfwWindowShouldClose(myWindow)) {
		//rendering
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shaderProgram);

		glDrawArrays(GL_TRIANGLES, 0, 3);

		//check and call events, swap buffers
		glfwPollEvents();
		glfwSwapBuffers(myWindow);
	}

	//delete everything
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);

	glfwTerminate();
	return 0;
}
