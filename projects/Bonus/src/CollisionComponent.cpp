#include "CollisionComponent.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "BulletTransform.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/wrap.hpp>

template <typename T>
T wrap(const T& value, const T& min, const T& max) {
	//(((x - x_min) % (x_max - x_min)) + (x_max - x_min)) % (x_max - x_min) + x_min;
	T range = max - min;
	return glm::mod(glm::mod(value - min, range) + range, range) + min;
}


void CollisionComponent::Update(entt::entity entity)
{
	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();

	auto view = CurrentRegistry().view<BulletTransform>();

	for (const auto& entityA : view){

		auto& transformA = CurrentRegistry().get<florp::game::Transform>(entity);

		if (entityA != entity) {
			if (transformA.GetLocalPosition() == transform.GetLocalPosition()) {
				CurrentRegistry().destroy(entity);
			}
		}

	}

}
