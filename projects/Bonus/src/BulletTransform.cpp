#include "BulletTransform.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "CollisionComponent.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/wrap.hpp>

template <typename T>
T wrap(const T& value, const T& min, const T& max) {
	//(((x - x_min) % (x_max - x_min)) + (x_max - x_min)) % (x_max - x_min) + x_min;
	T range = max - min;
	return glm::mod(glm::mod(value - min, range) + range, range) + min;
}

//void BulletTransform::OnLoad(entt::entity entity)
//{
//	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
//	// TODO:: ooo ooo ahh ahh
//	AudioEngine& audioEngine = AudioEngine::GetInstance();
//
//	audioEngine.LoadEvent("bullet");
//}

void BulletTransform::Update(entt::entity entity)
{
	using namespace florp::app;
	auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
	Window::Sptr window = Application::Get()->GetWindow();
	
	glm::vec3 translate = glm::vec3(0.0f);

	if (check) {
		translate.x -= 50.0f;
		//AudioEngine::GetInstance().PlayEvent("bullet");
	}
	if (window->IsKeyDown(Key::A) && check == false)
		translate.z += 12.f;
	if (window->IsKeyDown(Key::D) && check == false)
		translate.z -= 12.f;
	if (window->IsKeyDown(Key::Space)) {
		check = true;
	}

	translate *= Timing::DeltaTime * mySpeed;


	if (glm::length(translate) > 0) {
		translate = glm::mat3(transform.GetLocalTransform()) * translate;
		translate += transform.GetLocalPosition();
		transform.SetPosition(translate);
	}

}
