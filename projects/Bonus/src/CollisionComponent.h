#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class CollisionComponent : public florp::game::IBehaviour {
public:
	CollisionComponent(const glm::vec3& speed) : IBehaviour(), mySpeed(speed) {};
	virtual ~CollisionComponent() = default;

	//virtual void OnLoad(entt::entity entity) override;

	virtual void Update(entt::entity entity) override;

private:
	glm::vec3 mySpeed;
	bool check = false;
};
