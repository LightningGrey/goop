#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class BulletTransform : public florp::game::IBehaviour {
public:
	BulletTransform(const glm::vec3& speed) : IBehaviour(), mySpeed(speed){};
	virtual ~BulletTransform() = default;

	//virtual void OnLoad(entt::entity entity) override;

	virtual void Update(entt::entity entity) override;

private:
	glm::vec3 mySpeed;
	bool check = false;
};  
