#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "AudioEngine.h"

class AudioMovementBehaviour : public florp::game::IBehaviour {
public:
	AudioMovementBehaviour() : IBehaviour(), newPos(glm::vec3(0)), angle(4.7), 
		radius(10), angleIncrement(1), radiusIncrement(5), 
		isAngleMoving(false), isRadiusMoving(false) { };
	virtual ~AudioMovementBehaviour() = default;

	virtual void OnLoad(entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		// TODO:: ooo ooo ahh ahh
		AudioEngine& audioEngine = AudioEngine::GetInstance();

		audioEngine.LoadEvent("move");
		audioEngine.PlayEvent("move");
	}

	virtual void Update(entt::entity entity) override {
		using namespace florp::app;
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		Window::Sptr window = Application::Get()->GetWindow();

		//AudioEngine::GetInstance().PlayEvent("move");

	}

private:
	glm::vec3 newPos;
	bool isAngleMoving;
	bool isRadiusMoving;

	float angleIncrement;
	float radiusIncrement;

	float angle;
	float radius;
};