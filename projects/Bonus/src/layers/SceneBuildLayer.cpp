#include "SceneBuildLayer.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>

#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include <florp\game\Transform.h>
#include "RotateBehaviour.h"
#include "CameraComponent.h"
#include "florp/app/Application.h"
#include <ControlBehaviour.h>
#include "ControlAuto.h"
#include "BulletTransform.h"
#include <ShadowLight.h>
#include "PointLightComponent.h"
#include "LightFlickerBehaviour.h"
#include "AudioMovementBehaviour.h"
#include "CollisionComponent.h"

/*
 * Helper function for creating a shadow casting light
 * @param scene The scene to create the light in
 * @param entityOut An optional pointer to receive the ENTT entity that was created (set to nullptr if you don't care)
 * @param pos The position of the light in world space
 * @param target The point for the light to look at, in world space
 * @param up A unit vector indicating what axis is considered 'up'
 * @param distance The far clipping plane of the light
 * @param fov The field of view of the light, in degrees
 * @param bufferSize The size of the buffer to create for the light (default 1024x1024)
 * @param name The name to associate with the light's buffer
 */
ShadowLight& CreateShadowCaster(florp::game::Scene* scene, entt::entity* entityOut, glm::vec3 pos, glm::vec3 target, glm::vec3 up, float distance = 10.0f, float fov = 60.0f, glm::ivec2 bufferSize = { 1024, 1024 }, const char* name = nullptr)
{
	// The depth attachment is a texture, with 32 bits for depth
	RenderBufferDesc depth = RenderBufferDesc();
	depth.ShaderReadable = true;
	depth.Attachment = RenderTargetAttachment::Depth;
	depth.Format = RenderTargetType::Depth32;

	// Our shadow buffer is depth-only
	FrameBuffer::Sptr shadowBuffer = std::make_shared<FrameBuffer>(bufferSize.x, bufferSize.y);
	shadowBuffer->AddAttachment(depth);
	shadowBuffer->Validate();
	if (name != nullptr)
		shadowBuffer->SetDebugName(name);

	// Create a new entity
	entt::entity entity = scene->CreateEntity();

	// Assign and initialize a shadow light component
	ShadowLight& light = scene->Registry().assign<ShadowLight>(entity);
	light.ShadowBuffer = shadowBuffer;
	light.Projection = glm::perspective(glm::radians(fov), (float)bufferSize.x / (float)bufferSize.y, 0.25f, distance);
	light.Attenuation = 1.0f / distance;
	light.Color = glm::vec3(1.0f);

	// Assign and initialize the transformation
	florp::game::Transform& t = scene->Registry().get<florp::game::Transform>(entity);
	t.SetPosition(pos);
	t.LookAt(target, up);

	// Send out the entity ID if we passed in a place to store it
	if (entityOut != nullptr)
		*entityOut = entity;

	return light;
}

florp::graphics::Texture2D::Sptr CreateSolidTexture(glm::vec4 color)
{
	using namespace florp::graphics;
	static std::unordered_map<glm::vec4, Texture2D::Sptr> cache;

	// If a texture for that color exists in the cache, return it
	if (cache.find(color) != cache.end())
		return cache[color];
	// Otherwise, we'll create a new texture, cache it, then return it
	else {
		// We'll disable essentially anything fancy for our single-pixel color
		Texture2dDescription desc = Texture2dDescription();
		desc.Width = desc.Height = 1;
		desc.Format = InternalFormat::RGBA8;
		desc.MagFilter = MagFilter::Nearest;
		desc.MinFilter = MinFilter::Nearest;
		desc.MipmapLevels = 1;
		desc.WrapS = desc.WrapT = WrapMode::ClampToEdge;

		// By using the float pixel type, we can simply feed in the address of our color
		Texture2dData data = Texture2dData();
		data.Width = data.Height = 1;
		data.Format = PixelFormat::Rgba;
		data.Type = PixelType::Float;
		data.Data = &color.r;

		// Create the texture, and load the single pixel data
		Texture2D::Sptr result = std::make_shared<Texture2D>(desc);
		result->SetData(data);

		// Store in the cache
		cache[color] = result;
		
		return result;
	}
}

void SceneBuilder::Initialize()
{
	florp::app::Application* app = florp::app::Application::Get();

	using namespace florp::game;
	using namespace florp::graphics;

	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	// load objects
	MeshData ship = ObjLoader::LoadObj("ship.obj", glm::vec4(1.0f));
	MeshData shelter = ObjLoader::LoadObj("shelter.obj", glm::vec4(1.0f));
	MeshData invader = ObjLoader::LoadObj("space_invader.obj", glm::vec4(1.0f));
	MeshData bullet = ObjLoader::LoadObj("ship.obj", glm::vec4(1.0f));
	MeshData ui = ObjLoader::LoadObj("plane.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/forward.fs.glsl");
	shader->Link();

	// This is our emissive lighting shader
	Shader::Sptr emissiveShader = std::make_shared<Shader>();
	emissiveShader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	emissiveShader->LoadPart(ShaderStageType::FragmentShader, "shaders/forward-emissive.fs.glsl");
	emissiveShader->Link();

	// Creating our ship
	{
		// Material for the ship
		Material::Sptr shipMat = std::make_shared<Material>(shader);
		shipMat->Set("s_Albedo", Texture2D::LoadFromFile("ship.png", false, true, true));


		// Create the entity, attach renderable, set position
		entt::entity shipE = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(shipE);
		renderable.Material = shipMat;
		renderable.Mesh = MeshBuilder::Bake(ship);
		//Transform& t = scene->Registry().get<Transform>(shipE);
		//t.SetPosition(glm::vec3(0.0f, -1.0f, -12.0f));
		//t.SetEulerAngles(glm::vec3(90.0f, 0.0f, 0.0f));
		//t.SetScale(glm::vec3(0.5f));

	
		entt::entity shipLight = scene->CreateEntity();
		PointLightComponent& light = scene->Registry().assign<PointLightComponent>(shipLight);
		light.Color = glm::vec3(1.0f, 1.0f, 1.0f);
		light.Attenuation = 1.0f / 5.0f;
		Transform& tr = scene->Registry().get<Transform>(shipLight);
		tr.SetPosition(glm::vec3(0.0f, -1.0f, -12.0f));

		// We'll add our control behaviour so that we can fly the camera around
		scene->AddBehaviour<ControlBehaviour>(shipE, glm::vec3(1.0f));

		auto& shipTransform = scene->Registry().get<Transform>(shipE);
		shipTransform.SetScale(glm::vec3(0.5f));
		shipTransform.SetEulerAngles(glm::vec3(90.0f, 0.0f, 0.0f));
		shipTransform.SetPosition(glm::vec3(0.0f, -1.0f, -12.0f));



		// Creating our bullet
		// Material for the bullet
		Material::Sptr bulletMat = std::make_shared<Material>(shader);
		bulletMat->Set("s_Albedo", Texture2D::LoadFromFile("ship.png", false, true, true));

		// Create the entity, attach renderable, set position
		entt::entity bulletE = scene->CreateEntity();
		RenderableComponent& renderableB = scene->Registry().assign<RenderableComponent>(bulletE);
		renderableB.Material = bulletMat;
		renderableB.Mesh = MeshBuilder::Bake(bullet);

		entt::entity bulletLight = scene->CreateEntity();
		PointLightComponent& lightB = scene->Registry().assign<PointLightComponent>(bulletLight);
		lightB.Color = glm::vec3(1.0f, 1.0f, 1.0f);
		lightB.Attenuation = 1.0f / 5.0f;
		Transform& tl = scene->Registry().get<Transform>(bulletLight);
		tl.SetPosition(glm::vec3(0.0f, -1.0f, -12.0f));

		scene->AddBehaviour<BulletTransform>(bulletE, glm::vec3(1.0f));
		auto& bTransform = scene->Registry().get<Transform>(bulletE);
		bTransform.SetScale(glm::vec3(0.2f));
		bTransform.SetEulerAngles(glm::vec3(0.0f, 90.0f, 0.0f));
		bTransform.SetPosition(glm::vec3(0.0f, -1.0f, -12.0f));

	}

	{
		// Creating our shelter
		// Material for the shelter
		Material::Sptr shelterMat = std::make_shared<Material>(shader);
		shelterMat->Set("s_Albedo", Texture2D::LoadFromFile("shelter.png", false, true, true));

		// Create the entity, attach renderable, set position
		entt::entity shelterE = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(shelterE);
		renderable.Material = shelterMat;
		renderable.Mesh = MeshBuilder::Bake(shelter);
		Transform& t = scene->Registry().get<Transform>(shelterE);
		t.SetPosition(glm::vec3(0.0f, -1.0f, -6.0f));
		t.SetEulerAngles(glm::vec3(0.0f, 90.0f, 0.0f));
		t.SetScale(glm::vec3(1.5f));

		entt::entity shelterLight = scene->CreateEntity();
		PointLightComponent& light = scene->Registry().assign<PointLightComponent>(shelterLight);
		light.Color = glm::vec3(1.0f, 1.0f, 1.0f);
		light.Attenuation = 1.0f / 5.0f;
		Transform& tr = scene->Registry().get<Transform>(shelterLight);
		tr.SetPosition(glm::vec3(0.0f, -1.0f, -6.0f));

		entt::entity lightEnt = entt::null;
		auto& shadow = CreateShadowCaster(
			scene, &lightEnt, 
			glm::vec3(0.0f, -1.0f, 1.0f), // Each light will be behind the monkey
			glm::vec3(0.f),                                            // Look at the center
			glm::vec3(0.0f, 0.0f, 1.0f),                                              // Y is up
			25.0f,                                                                   // The far plane is 25 units away
			30.0f);                                                                  // We'll use a 75 degree field of view
		// We'll generate a color for the light
		shadow.Color = glm::vec3(1.0f, 0.64f, 0.0f) * 0.2f;
		shadow.Attenuation = 1.0f / 25.0f;

	}

	{
		// Creating our invader
		// Material for the invader
		Material::Sptr invaderMat = std::make_shared<Material>(shader);
		invaderMat->Set("s_Albedo", Texture2D::LoadFromFile("space_invader.png", false, true, true));

		// Create the entity, attach renderable, set position
		entt::entity invaderE = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(invaderE);
		renderable.Material = invaderMat;
		renderable.Mesh = MeshBuilder::Bake(invader);
		Transform& t = scene->Registry().get<Transform>(invaderE);
		t.SetPosition(glm::vec3(0.0f, -1.0f, 10.0f));
		t.SetEulerAngles(glm::vec3(0.0f, 180.0f, 0.0f));
		t.SetScale(glm::vec3(1.0));

		entt::entity invaderLight = scene->CreateEntity();
		PointLightComponent& light = scene->Registry().assign<PointLightComponent>(invaderLight);
		light.Color = glm::vec3(1.0f, 1.0f, 1.0f);
		light.Attenuation = 1.0f / 5.0f;
		Transform& tr = scene->Registry().get<Transform>(invaderLight);
		tr.SetPosition(glm::vec3(0.0f, -1.0f, 10.0f));

		// We'll add our control behaviour so that we can fly the camera around
		scene->AddBehaviour<ControlAuto>(invaderE, glm::vec3(1.0f));
		auto& invTransform = scene->Registry().get<Transform>(invaderE);
		invTransform.SetPosition(glm::vec3(0.0f, -1.0f, 10.0f));
		invTransform.LookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1));
		scene->AddBehaviour<CollisionComponent>(invaderE, glm::vec3(1.0f));



		entt::entity lightEnt = entt::null;
		auto& shadow = CreateShadowCaster(
			scene, &lightEnt, 
			glm::vec3(0.0f, -1.0f, 12.0f), // Each light will be behind the monkey
			glm::vec3(0.0f),                                                         // Look at the center
			glm::vec3(0.0f, 0.0f, 1.0f),                                              // Y is up
			25.0f,                                                                   // The far plane is 25 units away
			75.0f);                                                                  // We'll use a 75 degree field of view
		// We'll generate a color for the light
		shadow.Color = glm::vec3(1.0f, 0.64f, 0.0f) * 0.2f;
		shadow.Attenuation = 1.0f / 5.0f;
	}


	//// We'll use a constant to tell us how many monkeys to use
	//const int numMonkeys = 6;
	//const float step = glm::two_pi<float>() / numMonkeys; // Determine the angle between monkeys in radians
	//
	//// We'll create a ring of monkeys
	//for (int ix = 0; ix < numMonkeys; ix++) {
	//	entt::entity test = scene->CreateEntity();
	//	RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
	//	renderable.Mesh = MeshBuilder::Bake(data);
	//	renderable.Material = monkeyMat;
	//	Transform& t = scene->Registry().get<Transform>(test);
	//	t.SetPosition(glm::vec3(glm::cos(step * ix) * 5.0f, 0.0f, glm::sin(step * ix) * 5.0f));
	//	t.SetEulerAngles(glm::vec3(-90.0f, glm::degrees(-step * ix), 0.0f));
	//}
	//
	//// We'll create a ring of point lights behind each monkey
	//for (int ix = 0; ix < numMonkeys; ix++) {
	//	// We'll attach an indicator cube to all the lights, and align it with the light's facing
	//	entt::entity entity = scene->CreateEntity();
	//	PointLightComponent& light = scene->Registry().assign<PointLightComponent>(entity);
	//	light.Color = glm::vec3(
	//		glm::sin(-ix * step) + 1.0f, 
	//		glm::cos(-ix * step) + 1.0f, 
	//		glm::sin((-ix * step) + glm::pi<float>()) + 1.0f) / 2.0f * 0.1f;
	//	light.Attenuation = 1.0f / 10.0f;
	//	Transform& t = scene->Registry().get<Transform>(entity);
	//	t.SetPosition(glm::vec3(glm::cos(step * ix) * 20.0f, 2.0f, glm::sin(step * ix) * 20.0f));
	//	scene->AddBehaviour<LightFlickerBehaviour>(entity, 2.0f, 0.6f, 1.2f);
	//}
	//
	//// The central monkey
	//{
	//	entt::entity test = scene->CreateEntity();
	//	RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
	//	renderable.Mesh = MeshBuilder::Bake(data);
	//	renderable.Material = monkeyMat;
	//	Transform& t = scene->Registry().get<Transform>(test);
	//	// Make our monkeys spin around the center
	//	scene->AddBehaviour<RotateBehaviour>(test, glm::vec3(45.0f, 45.0f, 45.0f));
	//}
	

	//// We'll use a tiny cube to cast a shadow from our camera, and to indicate where the light sources are
	//MeshData indicatorCube = MeshBuilder::Begin();
	//MeshBuilder::AddAlignedCube(indicatorCube, glm::vec3(0.0f, 0, 0.0), glm::vec3(0.1f, 0.1f, 0.1f));
	//Mesh::Sptr indicatorMesh = MeshBuilder::Bake(indicatorCube);
		
	// Creates our main camera
	{
		// The color buffer should be marked as shader readable, so that we generate a texture for it
		RenderBufferDesc mainColor = RenderBufferDesc();
		mainColor.ShaderReadable = true;
		mainColor.Attachment = RenderTargetAttachment::Color0;
		mainColor.Format = RenderTargetType::ColorRgb8;

		// The normal buffer
		RenderBufferDesc normalBuffer = RenderBufferDesc();
		normalBuffer.ShaderReadable = true;
		normalBuffer.Attachment = RenderTargetAttachment::Color1;
		normalBuffer.Format = RenderTargetType::ColorRgb10; // Note: this format is 10 bits per component

		//// Create the descriptor for the emissive buffer
		//static_assert(false);
		RenderBufferDesc emissiveBuffer = RenderBufferDesc();
		emissiveBuffer.ShaderReadable = true;
		emissiveBuffer.Attachment = RenderTargetAttachment::Color2;
		emissiveBuffer.Format = RenderTargetType::ColorRgb10; // Note: this format is 10 bits per component

		
		// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
		RenderBufferDesc depth = RenderBufferDesc();
		depth.ShaderReadable = true;
		depth.Attachment = RenderTargetAttachment::Depth;
		depth.Format = RenderTargetType::Depth32;

		// Our main frame buffer needs a color output, and a depth output
		FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight(), 4);
		buffer->AddAttachment(mainColor);
		buffer->AddAttachment(normalBuffer);
		//static_assert(false); // Attach the emissive buffer to the G-Buffer
		buffer->AddAttachment(emissiveBuffer);
		buffer->AddAttachment(depth);
		buffer->Validate();
		buffer->SetDebugName("MainBuffer");

		// We'll create an entity, and attach a camera component to it
		entt::entity camera = scene->CreateEntity();
		CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
		cam.BackBuffer = buffer;
		cam.FrontBuffer = buffer->Clone();
		cam.IsMainCamera = true;
		cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 1000.0f);

		auto& camTransform = scene->Registry().get<Transform>(camera);
		camTransform.SetPosition(glm::vec3(0.0f, 30.0f, 0.0f));
		camTransform.LookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1));

		//// We'll attach a cube to the camera so that it casts shadows
		//RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(camera);
		//renderable.Mesh = indicatorMesh;
		//renderable.Material = marbleMat;

	}

	//// We'll use a constant to tell us how many monkeys to use
	//const int numMonkeys = 6;
	//const float step = glm::two_pi<float>() / numMonkeys; // Determine the angle between monkeys in radians
	//
	//// We'll create a ring of shadow casting lights, one for each monkey
	//for (int ix = 0; ix < numMonkeys; ix++) {
	//	entt::entity lightEnt = entt::null;
	//	auto& light = CreateShadowCaster(
	//		scene, &lightEnt, 
	//		glm::vec3(glm::cos(step * ix) * 9.0f, 3.5f, glm::sin(step * ix) * 9.0f), // Each light will be behind the monkey
	//		glm::vec3(0.0f),                                                         // Look at the center
	//		glm::vec3(0.0f, 1.0f, 0.0f),                                             // Y is up
	//		25.0f,                                                                   // The far plane is 25 units away
	//		75.0f);                                                                  // We'll use a 75 degree field of view
	//	// We'll generate a color for the light
	//	light.Color = glm::vec3(1.0f, 0.64f, 0.0f) * 0.2f; 
	//	light.Attenuation = 1.0f / 5.0f;
	//	scene->AddBehaviour<LightFlickerBehaviour>(lightEnt, 5.0f, 0.5f, 1.0f);
	//
	//	//// We'll attach an indicator cube to all the lights, and align it with the light's facing
	//	//entt::entity entity = scene->CreateEntity();
	//	//RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
	//	//renderable.Mesh = indicatorMesh;
	//	//renderable.Material = marbleMat;
	//	//Transform& t = scene->Registry().get<Transform>(entity);
	//	//t.SetPosition(glm::vec3(glm::cos(step * ix) * 9.0f, 2.0f, glm::sin(step * ix) * 9.0f));
	//}
			
	// Our floor plane
	{
		// Material for the ship
		Material::Sptr uiMat = std::make_shared<Material>(shader);
		uiMat->Set("s_Albedo", Texture2D::LoadFromFile("plane.png", false, true, true));

		// Create the entity, attach renderable, set position
		entt::entity planeE = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(planeE);
		renderable.Material = uiMat;
		renderable.Mesh = MeshBuilder::Bake(ui);
		Transform& t = scene->Registry().get<Transform>(planeE);
		t.SetScale(glm::vec3(3.5f));
		t.SetPosition(glm::vec3(0.0f, -10.0f, 0.0f));
		t.SetEulerAngles(glm::vec3(0.0f, 0.0f, 0.0f));
	}
}
