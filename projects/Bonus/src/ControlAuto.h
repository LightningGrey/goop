#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class ControlAuto : public florp::game::IBehaviour {
public:
	ControlAuto(const glm::vec3& speed) : IBehaviour(), mySpeed(speed) {};
	virtual ~ControlAuto() = default;

	virtual void Update(entt::entity entity) override;

private:
	glm::vec3 mySpeed;
	float xpos = 0.0f;
	float increment = 2.0f;
};
